Ext.define('ViewToMe.model.Content', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: 'name',
        fields: [
            'name','type'
        ]
    }
});
