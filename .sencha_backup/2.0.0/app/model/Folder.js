Ext.define('ViewToMe.model.Folder', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: 'identifier',
        fields: [
            'identifier','name','type'
        ]
    }
});
