Ext.define('ViewToMe.store.Contents', {
    extend: 'Ext.data.Store',
    requires: ['Ext.data.proxy.Rest'],
    config: {
        model: 'ViewToMe.model.Content',
        proxy: {
            type: 'rest',
            url : '/servetome/contents',
            noCache: false,
            limitParam: false,
            enablePagingParams: false,
            startParam: false,
            reader: {
                type: 'json'
            }
        }
    }
});

