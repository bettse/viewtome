Ext.define('ViewToMe.controller.Main', {
    extend: 'Ext.app.Controller',

    requires: ['Ext.Video'],
    config: {
        models: ['Folder', 'Content'],
        stores: ['Folders', 'Contents'],
        views: ['Main'],
        refs: {
        },
        control: {
            'main list': {
                itemtap: 'folderSelect'
            },
            'navigationview': {
                pop: 'viewPop'
            }
        }
    },

    constructor: function(config){
        this.initConfig(config);
        return this;
    },

    //called when the Application is launched, remove if not needed
    launch: function(app) {
        var me = this;
    },

    folderSelect: function(view, index, target, record, e, eOpts){
        var nav, path, folder, url;
        nav = view.up('navigationview');
        if(record.get('type') == 'folder'){
            folder = (record.get('identifier') !== undefined) ? record.get('identifier') : record.get('name');
            path = (view['path'] !== undefined) ? view.path + '/' + folder : folder;
            var contents = Ext.getStore('Contents').load({params: {path: path}});
            //console.log("going to push:", path);
            nav.push({
                xtype: 'list',
                title : record.get('name'),
                path : path,
                store: contents,
                scrollable: true,
                clearSelectionOnDeactivate: true,
                itemTpl: '<div class="item-{type}"><img src="resources/images/{type}.png" style="height:20px;width:20px;padding-right:3px;"/>{name}</div>',
                onItemDisclosure: function() {}
            });
        } else {
            path = view.path + '/' + record.get('name');
            url = '/servetome/stream/device=iPhone4s,rate=3g,aud=any,sub=any,ss=0,gain=0,art=yes,dir=default,enc=Auto,pdur=30,trans=both/' + path + '/index.m3u8?offset=0.000000';
            console.log(url);
            window.open(url);
        }
    },

    viewPop: function(objNavView, objView, eOpts){
        objView.destroy();
        var path = objNavView.getActiveItem().path;
        //If we *aren't* back at the original "Folders" view
        if(path !== undefined && path !== '' && path !== '/'){
            Ext.getStore('Contents').load({
                params: {
                    path: path
                }
            });
        }
    }

});
