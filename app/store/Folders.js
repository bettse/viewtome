Ext.define('ViewToMe.store.Folders', {
    extend: 'Ext.data.Store',
    requires: [],
    config: {
        model: 'ViewToMe.model.Folder',
        proxy: {
            type: 'ajax',
            url : '/servetome/folders',
            noCache: false,
            limitParam: false,
            enablePagingParams: false,
            startParam: false
        },
        autoLoad: true
    }
});

