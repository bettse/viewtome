Ext.define("ViewToMe.view.Main", {
    extend: 'Ext.navigation.View',
    requires: ['Ext.dataview.List'],
    xtype: 'main',
    config: {
        items: [{
            xtype: 'list',
            store: 'Folders',
            title: 'Folders',
            scrollable: true,
            clearSelectionOnDeactivate: true,
            itemTpl: '<div class="item-{type}"><img src="resources/images/{type}.png" style="height:20px;width:20px;padding-right:3px;"/>{name}</div>',
            onItemDisclosure: function() {}
        }]
    }
});

