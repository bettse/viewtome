Ext.define('ViewToMe.Override', {});

Ext.define('ViewToMe.Override.JsonReader', {
    override: 'Ext.data.reader.Json',
    getResponseData: function(response) {
        var responseText = response;
        // Handle an XMLHttpRequest object
        if (response && response.responseText) {
            responseText = response.responseText;
        }

        // Handle the case where data has already been decoded
        if (typeof responseText !== 'string') {
            return responseText;
        }

        var index = responseText.indexOf('Data-Length');
        if(index != -1){
            var crlf = responseText.indexOf('\n', index);
            responseText = responseText.substr(crlf);
        }

        var data;
        try {
            data = Ext.decode(responseText);
        }
        catch (ex) {
            /**
             * @event exception Fires whenever the reader is unable to parse a response.
             * @param {Ext.data.reader.Xml} reader A reference to this reader
             * @param {XMLHttpRequest} response The XMLHttpRequest response object.
             * @param {String} error The error message.
             */
            this.fireEvent('exception', this, response, 'Unable to parse the JSON returned by the server: ' + ex.toString());
            Ext.Logger.warn('Unable to parse the JSON returned by the server: ' + ex.toString());
        }
        //<debug>
        if (!data) {
            this.fireEvent('exception', this, response, 'JSON object not found');

            Ext.Logger.error('JSON object not found');
        }
        //</debug>
        return data;
    }
});
Ext.define('ViewToMe.Override.ContentsProxy', {
    override: 'Ext.data.proxy.Rest',
    buildUrl: function(request) {
        var me        = this,
            operation = request.getOperation(),
            records   = operation.getRecords() || [],
            record    = records[0],
            model     = me.getModel(),
            idProperty= model.getIdProperty(),
            format    = me.getFormat(),
            url       = me.getUrl(request),
            params    = request.getParams() || {},
            id        = (record && !record.phantom) ? record.getId() : params[idProperty];

        if (me.getAppendId() && id) {
            if (!url.match(/\/$/)) {
                url += '/';
            }
            url += id;
            delete params[idProperty];
        }

        if(params['path'] !== undefined){
            if (!url.match(/\/$/)) {
                url += '/';
            }

            url += params['path'];
            delete params['path'];
        }

        if (format) {
            if (!url.match(/\.$/)) {
                url += '.';
            }

            url += format;
        }

        request.setUrl(url);

        return me.callParent([request]);

    }
});

