#!/bin/bash

FILES=`find app -iname "*.js" | tr '\n' ','`

sencha fs concat -t=target.js -f=app.js,$FILES

sencha fs minify --from target.js --to app-minified.js --compressor closurecompiler
#compass compile resources/sass

